/**
 * Created by ntakallapaly on 11/5/2018.
 */
({
    doinit : function(component,event,helper){
        helper.getOrgTypesChart(component, event, helper);
        component.set("v.showTable",false);
        var fl = 'Total Commitments in '+ component.get("v.recordName");
        
        component.set('v.ownerListColumns', [
            {label: 'Sales Person', fieldName: 'Owner', type: 'text', sortable: true},
            {label: 'Commitments in Product', fieldName: 'Amount', type: 'currency', sortable: true, typeAttributes: { currencyCode: 'USD' }, cellAttributes: { alignment: 'left' }}
        ]);
        component.set('v.accsListColumns', [
            {label: 'Organization', fieldName: 'ref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'Name', target: '_blank' }
                }
            },
            // {label: 'Owner', fieldName: 'Owner', type: 'text', sortable: true},
            {label: 'Secondary Coverage', fieldName: 'SC', type: 'text', sortable: true},
            {label: 'Region', fieldName: 'Region', type: 'text', sortable: true},
            {label: 'Commitments in Product', fieldName: 'orginvAmt', type: 'currency', sortable: true, typeAttributes: { currencyCode: 'USD' }, cellAttributes: { alignment: 'left' }}
        ]);

        component.set('v.invsListColumns', [
            {label: 'Organization', fieldName: 'orgref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'Org', target: '_blank' }
                }
            },
            //{label: 'Owner', fieldName: 'Owner', type: 'text', sortable: true},
            {label: 'Investment', fieldName: 'ref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'Name', target: '_blank' }
                }
            },
            {label: 'Investor Type', fieldName: 'Investor_Type__c', type: 'text', sortable: true},
            {label: 'Fund', fieldName: 'fndref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'Fund', target: '_blank' }
                },
                actions: {label: 'WRAP TEXT',checked: true}
            },
            {label: 'Commitment USD', fieldName: 'Current_Commitment_USD__c', type: 'currency', sortable: true, typeAttributes: { currencyCode: 'USD' }, cellAttributes: { alignment: 'left' }}
        ]);

        component.set('v.transListColumns', [
            {label: 'Transaction', fieldName: 'ref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'Name', target: '_blank' }
                }
            },
            {label: 'Investment', fieldName: 'invref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'InvName', target: '_blank' }
                }
            },
            {label: 'Opportunity', fieldName: 'oppref', type: 'url', sortable: true,
                typeAttributes: {
                    label: { fieldName: 'OppName', target: '_blank' }
                }
            },
            {label: 'Opp Owner', fieldName: 'OppOwner', type: 'text', sortable: true},
            {label: 'Transaction Date', fieldName: 'Transaction_Date__c', type: 'date', sortable: true},
            {label: 'Commitment USD', fieldName: 'Amount_USD__c', type: 'currency', sortable: true, typeAttributes: { currencyCode: 'USD' }, cellAttributes: { alignment: 'left' }}
        ]);
    },

    backtoFndGrp : function(component,event,helper){
        window.location.href = "/"+component.get("v.recordId");
    },

    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },
    changeView: function(component, event, helper) {
        component.set("v.chartView",event.getSource().get("v.name"));
        //component.set("v.invsList",null);
        //component.set("v.showTable",false);
        helper.displayOrgTypesdata(component, event, helper);
    },

    changeTable: function(component, event, helper) {
        component.set("v.tableView",event.getParam("value"));
    },

    updateColumnSorting: function(component, event, helper) {

        var table = event.getSource().getLocalId();
        var fieldName = event.getParam('fieldName');
        var fieldType = event.getParam('type');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection, table,fieldType);

    },

    downloadDoc: function(component, event, helper) {
        var sendDataProc = component.get("v.sendData");
        var dataToSend = {
            "ProductName" : component.get("v.recordName"),
            "listType" : component.get("v.tableView"),
            "invType" : component.get("v.selectedType"),
            "totalCommitments" : component.get("v.totalCommitments"),
            "Owners" : component.get("v.ownerList"),
            "Orgs" : component.get("v.accsList"),
            "Invs" : component.get("v.invsList"),
            "Comms" : component.get("v.transList")
        };
        sendDataProc(dataToSend, function(){});
    },

})