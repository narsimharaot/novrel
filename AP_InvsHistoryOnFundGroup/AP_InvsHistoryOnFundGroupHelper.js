/**
 * Created by ntakallapaly on 11/5/2018.
 */
({
    getOrgTypesChart: function(component, event, helper) {
        var action = component.get("c.getInvHistory");
        action.setParams({"fndId" : component.get("v.recordId"), "rc" : component.get("v.rc"), "rcst" : component.get("v.rcst")});
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.response", outputresponse);
                this.displayOrgTypesdata(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },

     displayOrgTypesdata: function(component, event, helper) {
        var title = 'Organization Investment History ('+component.get("v.chartView")+')';
        var bcolor= ["#007D55","#3867B0","#FDAE2A","#888888", "#A6C773",'#FC8651',"#1BAFDC",  "#8ba9da", "#b1c6e7", "#d8e2f3", "#CF56E1",  "#ADE254", "#69E257", "#56E289", "#9eb8e0", "#00cc8b"];
        var dataValues = [];
        var typeLabels = [];
        var rlist = component.get("v.response");
        var ciew = component.get("v.chartView")
        for (var i = 0; i < rlist.length; i++) {
             typeLabels.push(rlist[i].invType);
             if(ciew === 'By Amount') {
                 dataValues.push(rlist[i].amtPercent);
             }else{
                dataValues.push(rlist[i].cntPercent);
             }
        }

        if(window.orgTypesChart!=undefined) {
            window.orgTypesChartOld = window.orgTypesChart;
            window.orgTypesChart.destroy();
        }
        this.getstatictp();
        window.orgTypesChart = new Chart(document.getElementById('orgType'), {
            type: 'doughnut',
            responsive: false,
            data: {
                labels: typeLabels,
                datasets: [{
                    backgroundColor: bcolor,
                    data: dataValues,
                    labelColor: 'white',
                    labelFontSize: '16',
                }]
            },
            options: {
                cutoutPercentage: 60,
                plugins: {
                    labels: {
                        render: 'value',
                        position: 'outside',
                        fontFamily: 'Segoe UI Semilight'
                    }
                },
                legend: {
                    labels: {
                        generateLabels: function(chart) {
                            var data = chart.data;
                            var helpers = Chart.helpers;
                            if (data.labels.length && data.datasets.length) {
                                return data.labels.map(function(label, i) {
                                    var meta = chart.getDatasetMeta(0);
                                    var ds = data.datasets[0];
                                    var arc = meta.data[i];
                                    var custom = arc && arc.custom || {};
                                    var getValueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;
                                    var arcOpts = chart.options.elements.arc;
                                    var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                    var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                    var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

                                    return {
                                        text: label +' ('+ds.data[i]+'%)',
                                        fillStyle: fill,
                                        strokeStyle: stroke,
                                        lineWidth: bw,
                                        hidden: isNaN(ds.data[i]) || meta.data[i].hidden,

                                        // Extra data used for toggling the correct item
                                        index: i
                                    };
                                });
                            }
                            return [];
                        },
                        fontFamily: 'Segoe UI',
                        fontSize: 12,
                        fontStyle: 'bold'
                    },
                    position: 'bottom',

                },
                title: {
                    display: true,
                    text: title,
                    fontFamily: 'Segoe UI',
                    fontSize: 18
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var name = data.labels[tooltipItem.index];
                            var Value = dataset.data[tooltipItem.index];
                            var currentValue = dataset.data[tooltipItem.index];
                            return name + ': '+Value + "%";
                        }
                    },
                    bodyFontFamily: 'Segoe UI '
                },
                onClick: $A.getCallback(function(event) {
                    var elements = window.orgTypesChart.getElementAtEvent(event);
                    if (elements.length === 1) {
                        var selType = elements[0]._model.label;
                        component.set("v.showTable",true);
                        component.set("v.tableView","Summary");
                        this.getInvList(component, event, helper, selType);
                    }

                }.bind(this)),
            }
        });

    },

    getInvList: function(component, event, helper, selType) {
        component.set("v.selectedType",selType);
        var rlist = component.get("v.response");
        var accs = [];
        var ownerSet = new Set();
        var ownerMap = new Map();
        var owners = [];
        var invs = [];
        var trans = [];
        for (var i = 0; i < rlist.length; i++) {
            if (rlist[i]['invType'] === selType) {
                invs = rlist[i].invList;
                accs = rlist[i].orgStats;
                trans = rlist[i].invTransList;
                component.set("v.totalCommitments", rlist[i].amt);
                component.set("v.amtPercent", rlist[i].amtPercent);
                component.set("v.totalInvs", rlist[i].cnt);
                component.set("v.cntPercent", rlist[i].cntPercent);
                component.set("v.totalCommitmentsFund", rlist[i].totalAmt);
                component.set("v.totalInvsFund", rlist[i].totalCnt);
            }
        }
        for (var i = 0; i < accs.length; i++) {
            var row = accs[i];

            if (row.orgName.Name){
                row.Name = row.orgName.Name;
                row.ref = '/'+row.orgName.Id;
            }
            if (row.orgName.Owner.Name) row.Owner = row.orgName.Owner.Name;
            if (row.orgName.Organization_Coverage__c) row.SC = row.orgName.Organization_Coverage__c;
            if (row.orgName.Region__c) row.Region = row.orgName.Region__c;
            
            if(ownerMap.get(row.Owner) == undefined){
                var ownObj = {"oAmnt": row.orginvAmt, "ownOrgs": [row]};
                ownerMap.set(row.Owner,ownObj);
            }else{
                var ownObj = ownerMap.get(row.Owner);
                ownObj.oAmnt = row.orginvAmt+ownObj.oAmnt;
                ownObj.ownOrgs.push(row);
                ownerMap.set(row.Owner,ownObj);
            }
            ownerSet.add(row.Owner);
        }
        
        for (let item of ownerSet) {

            var own = {"Owner":item,"Amount":ownerMap.get(item).oAmnt, "Orgs": ownerMap.get(item).ownOrgs};
            owners.push(own);
        }
        owners.sort(this.sortBy('Amount', true,'currency'));

        for (var i = 0; i < owners.length; i++) {
            var row = owners[i];
            if(row.Amount) {
                var amnt = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'USD'
                }).format(row.Amount);
                row.Amount = amnt;
            }
        }
        component.set("v.ownerList",owners);
        
        accs.sort(this.sortBy('orginvAmt', true,'currency'));
        component.set("v.accsList",accs);
        for (var i = 0; i < invs.length; i++) {
            var row = invs[i];
            row.ref = '/'+row.Id;
            if (row.Organization__c){
                row.Org = row.Organization__r.Name;
                row.Owner = row.Organization__r.Owner.Name;
                row.orgref = '/'+row.Organization__c;
            }
            if (row.Fund__c){
                row.Fund = row.Fund__r.Name;
                row.fndref = '/'+row.Fund__c;
            }
        }
        invs.sort(this.sortBy('Current_Commitment_USD__c', true,'currency'));
        component.set('v.invsList', invs);

        for (var i = 0; i < trans.length; i++) {
            var row = trans[i];
            row.ref = '/'+row.Id;
            if (row.Investment__c){
                row.InvName = row.Investment__r.Name;
                row.invref = '/'+row.Investment__c;
            }
            if (row.Opportunity__c){
                row.OppName = row.Opportunity__r.Name;
                row.oppref = '/'+row.Opportunity__c;
                row.OppOwner = row.Opportunity__r.Owner.Name;
            }
        }
        trans.sort(this.sortBy('Amount_USD__c', true,'currency'));
        component.set("v.transList",trans);
    },

    getstatictp : function(){
        Chart.plugins.register({
            afterDatasetsDraw: function(chartInstance, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chartInstance.chart.ctx;
                chartInstance.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'white';
                            var fontSize = 16;
                            var fontStyle = 'normal';
                            var fontFamily = 'Segoe UI';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();
                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            var padding = 5;
                            var position = element.tooltipPosition();
                            if(dataString == '1' || dataString == '0')
                                ctx.fillText('',position.x, position.y - (fontSize / 2) - padding);
                            else
                                ctx.fillText(dataString + '%', position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });
    },
    
    sortData: function (cmp, fieldName, sortDirection,table, fieldType) {
        var reverse = sortDirection !== 'asc';
        console.log('check dir: '+reverse);
        if(table=='accsTable'){
            var data = cmp.get("v.accsList");

            data.sort(this.sortBy(fieldName, reverse,fieldType))
            cmp.set("v.accsList", data);
        }if(table=='invsTable'){
            var data = cmp.get("v.invsList");

            data.sort(this.sortBy(fieldName, reverse,fieldType))
            cmp.set("v.invsList", data);
        }if(table=='transTable'){
            var data = cmp.get("v.transList");

            data.sort(this.sortBy(fieldName, reverse,fieldType))
            cmp.set("v.transList", data);
        }
    },

    sortBy: function (field, reverse, fieldType,primer) {
        var key = primer ?
            function(x) {return primer(x.hasOwnProperty(field) ? (typeof x[field] === 'string' ? x[field].toLowerCase() : x[field]) : 'aaa')} :
            function(x) {return x.hasOwnProperty(field) ? (typeof x[field] === 'string' ? x[field].toLowerCase() : x[field]) : 'aaa'};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },

})